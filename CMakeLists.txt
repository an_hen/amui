    cmake_minimum_required(VERSION 2.8)
    
    PROJECT(Amui)
    
    SET(CMAKE_MODULE_PATH  "./cmake")

# --------------------------------- Qt4 ----------------------------------------

    FIND_PACKAGE(Qt4 REQUIRED)
        #SET(QT_USE_QTNETWORK TRUE)
        #SET(QT_USE_PHONON TRUE)
    INCLUDE(${QT_USE_FILE})

# ------------------------------- Coin3D ---------------------------------------

    #FIND_PACKAGE(Coin3D REQUIRED)
    #FIND_PACKAGE(SoQt REQUIRED)
    # "Quarter"
    # There is a lib Quarter which in combinatioan with
    # Coin and Qt makes SoQt unnecessary.
    # Seems to be too minimal, no mouse-camera interaction.
    # But it is possible to integrate it.
    
# ------------------------------- Eigen ----------------------------------------

    #FIND_PACKAGE(Eigen3)

# --------------------------- Source files -------------------------------------

    # Implementation files. Header files not necessary.
    SET(AMUI_SRC_DIR ${CMAKE_CURRENT_LIST_DIR})

    SET(AMUI_SOURCES
        ${AMUI_SRC_DIR}/main.cpp
	)

    # Header files of classes which inherits QObject. This list in necessary for
    # moc-precompiler.
    SET(AMUI_HEADER
	)

    # User-interface files needed for Qt-ui-cpp-wrapper.
    #SET(OPENMMM_VISUALIZER_UI
    #    ${OPENMMM_VISUALIZER_DIR}/SceneVisualizationWidget.ui
    #)

    # Qt ressource files.
    #SET(OPENMMM_VISUALIZER_QRC
    #    ${OPENMMM_VISUALIZER_DIR}/images/images.qrc
	#)
        
# ----------------------- Qt-precompile-processing -----------------------------

    QT4_WRAP_UI(AMUI_UI_H ${AMUI_UI})
    SET(AUTOMOC TRUE)
    QT4_WRAP_CPP(AMUI_MOC ${AMUI_HEADER} )
    QT4_ADD_RESOURCES(AMUI_RCC ${AMUI_QRC} )
    
# ------------------------- Compiling and linking ------------------------------

    INCLUDE_DIRECTORIES(
        ${CMAKE_CURRENT_BINARY_DIR} 	#for AmuiConfigure.h
        ${Eigen3_INCLUDE_DIR}
        ${AMUI_SRC_DIR}
    )
        
    # ----------------- binary ----------------------------------------
    
    
    ADD_EXECUTABLE(Amui
        ${AMUI_UI_H}
        ${AMUI_MOC}
        ${AMUI_RCC}
        ${AMUI_SOURCES}
    )
    
    TARGET_LINK_LIBRARIES(Amui ${QT_LIBRARIES} ${Eigen3_LIBRARIES} ${SoQt_LIBRARIES} ${Coin3D_LIBRARIES} ${Coin3DUtils_LIBRARIES} ${COIN3D_LIBRARIES} ${OPENGL_LIBRARIES})
